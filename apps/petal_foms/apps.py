from django.apps import AppConfig


class PetalFomsConfig(AppConfig):
    name = 'petal_foms'
